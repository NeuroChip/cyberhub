#!/bin/bash

export PORT=7000

export REDIS_URL="redis://localhost:6397/0"
export MONGODB_URL="mongodb://localhost/prism"
export NEO4J_URL="http://localhost:7474"

