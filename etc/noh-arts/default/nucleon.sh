#!/bin/bash

if [[ "x"$RONIN_PARTs == "x" ]] ; then
    export RONIN_PARTs=( "console" "system" "language" "realm" "feature" )
fi

if [[ "x"$RONIN_LANGUAGEs == "x" ]] ; then
    export RONIN_LANGUAGEs=( "java" "go" "python" "nodejs" "hack" "ruby" )
fi

if [[ "x"$RONIN_REALMs == "x" ]] ; then
    export RONIN_REALMs=( "common" "connector" "console" "explor" "linked" "hobbit" "linguist" "devops" "infosec" )
fi

export NODE_VERSION="v7.9.0"

