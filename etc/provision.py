class RoninRealm(CrossRealm):
    def config(self):
        self.event('exact', 1000, self("logging"))
        self.queue('exact', 1000, self("logging"))

        #for key in os.environ.get('RONIN_REALMs','').split(' '):
        #    pass

    def setup(self):
        self.allow('backend',    "prefix", self(""))

        for entry in []:
            self.allow('device',     "prefix", self("dev."))
            self.allow('device',     "prefix", self(""), publish=False, register=False)

        self.allow('authorizer', "exact", self("authentify"), False, False, False, True)

        self.allow('frontend',   "prefix", self("live."))
        self.allow('frontend',   "prefix", self(""), publish=False, register=False)

        self.allow('anonymous',  "prefix", self("head."))
        self.allow('anonymous',  "prefix", self(""), False, False, False, False)

########################################################################################

#@click.option('--hash-type', type=click.Choice(['md5', 'sha1']))

def commandline(cli, cmd):
    yield click.option('--port', default=None)
    yield click.option('--mode', default='person', type=click.Choice([
        'agent','organism','person'
    ]))
    yield click.option('--root', default='html', type=click.Choice([
        'http','wsgi','html'
    ]))
    yield click.option('--job/--no-job', default=True)
    yield click.option('--rq', multiple=True)
    yield click.option('--svc/--no-svc', default=True)
    yield click.option('--tpf/--no-tpf', default=False)
    yield click.option('--ldp/--no-ldp', default=False)

    yield click.option('--ops/--no-ops', default=True)
    yield click.option('--sec/--no-sec', default=True)

#***************************************************************************************

def instantiate(**kwargs):
    return CrossHub(kwargs['path'])

#***************************************************************************************

def provision(hub, **kwargs):
    hub.build(RoninRealm, 'reflector')

    #*********************************************************************

    hub.static('assets', "usr/static/assets")
    hub.static('media',  "usr/static/media")
    hub.static('static', "usr/static/assets")

    if kwargs['root']=='wsgi':
        hub.wsgi('/', "reflector.wsgi", "app")
    elif kwargs['root']=='html':
        hub.static('/', "usr/static/portal")
    else: # elif kwargs['root']=='http':
        hub.proxy('/', "localhost", hub.ports['wsgi'], '/')

    #hub.upload('upload', 'reflector', 'nerves', hub.rpath('srv'), hub.rpath('srv'))

    #*********************************************************************

    from ronin.contrib.backends.Queues import RedisQueue

    if kwargs['job']:
        hub.install(RedisQueue.Dashboard)

    ind = 1

    for queue in kwargs['rq']:
        hub.install(RedisQueue.Worker, "internal-%d" % ind, *queue.split(','))

        ind += 1

    if kwargs['svc']:
        from ronin.contrib.frameworks.Supervisor.provision import Instance

        hub.install(Instance)

    #*********************************************************************

    if False:
        hub.install(TPF, None, "usr/daten/tpf",
            dict(),
        workers=2)

        hub.install(Solid, None, '.')

    #*********************************************************************

    hub.poll('poll')

    hub.socket('sock', 'json','msgpack','ubjson','cbor',
        ticket={
            "type": "dynamic",
            "authenticator": "reflector.authentify"
        },
    )

    #*********************************************************************

    from ronin.contrib.corporate.HashiCorp import provision as HCT

    if kwargs['ops']:
        hub.install(HCT.Consul)

        hub.install(HCT.Nomad,
            vault_addr="http://127.0.0.1:8200",
            vault_root="eeb36535-99b2-3fbe-f09c-3389fc0dada7",
        )

    if kwargs['sec']:
        hub.install(HCT.Vault)

