#!/bin/bash

################################################################################

ronin_load_system () {
    export TARGET_KEY=$1
    export TARGET_DIR=$RONIN_SYS_DIR/$TARGET_KEY

    if [[ ! -d $TARGET_DIR ]] ; then
        git clone https://bitbucket.org/IT-Bushido/$TARGET_KEY.git $TARGET_DIR
    fi

    if [[ ! -d $TARGET_DIR ]] ; then
        motd_text "Could'nt find the specs for your Operating System '"$RONIN_SYSTEM"' at : "$RONIN_SYS_DIRDIR

        motd_text "Sorry, the Shelter can't bootup. Exiting ..."

        motd_exit
    fi
}

#*******************************************************************************

ronin_resolve_system () {
    export TARGET_KEY=$1
    export TARGET_DIR=$RONIN_SYS_DIR/$TARGET_KEY

    DERIVEs=( )
    FLAVORs=( )

    while [[ -f $TARGET_DIR/context.sh ]] ; do
        source $TARGET_DIR/context.sh

        DERIVEs=($RONIN_SYSTEM_DERIVED "${DERIVEs[@]}")
        FLAVORs=("${FLAVORs[@]}" $RONIN_SYSTEM_FLAVORs)

        TARGET_DIR=$RONIN_SYS_DIR/$RONIN_SYSTEM_DERIVED
    done

    ronin_load_system $TARGET_KEY

    for TARGET_KEY in ${DERIVEs[@]} ; do
        ronin_load_system $TARGET_KEY
    done

    export RONIN_SYSTEM_DERIVEs=("${DERIVEs[@]}")
    export RONIN_SYSTEM_FLAVORs=("${FLAVORs[@]}")

    unset DERIVEs FLAVORs
}

################################################################################

ronin_require_system () {
    if [[ -f /etc/os-release ]] ; then
        source /etc/os-release

        for KEY in ID ID_LIKE NAME PRETTY_NAME VERSION VERSION_ID HOME_URL SUPPORT_URL BUG_REPORT_URL ANSI_COLOR ; do
            VALUE=`eval 'echo \$'$KEY`

            if [[ "$VALUE" != "" ]] ; then
                eval 'export RONIN_SYSTEM_'$KEY'="'$VALUE'"'
                eval 'export DISTRIB_'$KEY'="'$VALUE'"'

                #eval "export RONIN_SYSTEM_"$KEY"=\$"$KEY
                #eval "export DISTRIB_"$KEY"=\$"$KEY

                unset $KEY
            fi
        done
    fi

    #*******************************************************************************************************************************

    export RONIN_KERNEL_ARCH=x
    export RONIN_KERNEL_BITS=32

    if [[ -x /bin/uname ]] ; then
        export RONIN_KERNEL_NAME=`uname -s`

        export RONIN_KERNEL_MACHINE=`uname -m`
        export RONIN_KERNEL_PROCESS=`uname -p`

        export RONIN_KERNEL_RELEASE=`uname -r`
        export RONIN_KERNEL_VERSION=`uname -v`

        case $RONIN_KERNEL_MACHINE in
            amd64)
                export RONIN_ARCH=amd
                export RONIN_KERNEL_BITS=64
                ;;
            x86_64|ia64|i686-64)
                export RONIN_KERNEL_ARCH=intel
                export RONIN_KERNEL_BITS=64
                ;;
            i386|i686|i86pc)
                export RONIN_KERNEL_ARCH=intel
                export RONIN_KERNEL_BITS=32
                ;;
            armv6l)
                export RONIN_KERNEL_ARCH=arm6l
                export RONIN_KERNEL_BITS=32
                ;;
            armv7l)
                export RONIN_KERNEL_ARCH=arm7l
                export RONIN_KERNEL_BITS=32
                ;;
            arm64)
                export RONIN_KERNEL_ARCH=armh
                export RONIN_KERNEL_BITS=64
                ;;
            ppc64)
                export RONIN_KERNEL_ARCH=ppc
                export RONIN_KERNEL_BITS=64
                ;;
            ppc)
                export RONIN_KERNEL_ARCH=ppc
                export RONIN_KERNEL_BITS=32
                ;;
            sparc|sun4u)
                export RONIN_KERNEL_ARCH=sparc
                export RONIN_KERNEL_BITS=32
                ;;
            mips)
                export RONIN_KERNEL_ARCH=mips
                export RONIN_KERNEL_BITS=32
                ;;
            s390x)
                export RONIN_KERNEL_ARCH=s390x
                export RONIN_KERNEL_BITS=32
                ;;
        esac
    fi

    export RONIN_ARCH=$RONIN_KERNEL_ARCH$RONIN_KERNEL_BITS

    #*******************************************************************************************************************************

    case $DISTRIB_ID in
        raspbian)
            export RONIN_SYSTEM="raspbian"
            ;;
        debian|Debian)
            export RONIN_SYSTEM="debian"
            ;;
        ubuntu|Ubuntu)
            export RONIN_SYSTEM="ubuntu"
            ;;
        *)
            case $OSTYPE in
                cygwin)
                    export RONIN_SYSTEM="windows"

                    export RONIN_HOME_ROOT=$HOME
                    export RONIN_HOME_DIR=$HOMEDRIVE"\Users"
                    ;;
                *)
                    export RONIN_SYSTEM="linux"

                    if [[ -f /usr/bin/yum ]] ; then
                      export RONIN_SYSTEM="redhat"
                    fi

                    if [[ -d /Users ]] ; then
                      export RONIN_SYSTEM="macosx"

                      export RONIN_HOME_ROOT=/var/root
                      export RONIN_HOME_DIR=/Users
                    fi
                     ;;
            esac
            ;;
    esac

    #*******************************************************************************************************************************

    export RONIN_SYSDIR=$RONIN_SYS_DIR/$RONIN_SYSTEM

    ronin_resolve_system $RONIN_SYSTEM

    #*******************************************************************************

    export OS_NAME=$RONIN_SYSTEM
    export OS_PATH=$RONIN_SYS_DIRDIR

    for key in ID NAME VERSION ANSI_COLOR ; do
        if [[ "`eval 'echo \$RONIN_SYSTEM_'$key`" != "" ]] ; then
          eval "export OS_$key=\$RONIN_SYSTEM_$key"
        fi
    done

    for key in environ context ; do
        if [[ -f $RONIN_SYS_DIRDIR/$key.sh ]] ; then
            source $RONIN_SYS_DIRDIR/$key.sh
        fi
    done

    unset OS_NAME OS_PATH

    for key in ID NAME VERSION ANSI_COLOR ; do
        if [[ "`eval 'echo \$OS_'$key`" != "" ]] ; then
          eval "unset OS_"$key
        fi
    done
}

#*******************************************************************************

ronin_include_system () {
    motd_text " "
    motd_text "Using platform :"
    motd_text "    Hostname : "`hostname -f`
    motd_text "    Invite   : "$RONIN_SHELL_INVITE
    motd_text "    Login    : "$USER
    #motd_text "    Module   : "$RONIN_MODULE
    motd_text "    System   : "$RONIN_SYSTEM
    motd_text "    -> based-on : "$RONIN_SYSTEM_DERIVEs
    motd_text "    -> flavors  : "$RONIN_SYSTEM_FLAVORs
}

################################################################################################################################

ronin_setup_system () {
    if [ $RONIN_OS=="windows" ] ; then
        export CYG_PKGs=$CYG_PKGs",nano"
        export CYG_PKGs=$CYG_PKGs",bash,dash,fish,mksh,zsh"
        export CYG_PKGs=$CYG_PKGs",tree,htop,nmap"
    fi

    if [ $RONIN_OS=="macosx" ] ; then
        export BREW_PKGs=$BREW_PKGs" mksh"
    fi

    if [ $RONIN_OS=="debian" ] ; then
        export APT_PKGs=$APT_PKGs" mksh zsh zsh-lovers"
        export APT_PKGs=$APT_PKGs" tree htop fping nmap traceroute"
    fi

    export NPM_PKGs="$NPM_PKGs nsh"
}

##*******************************************************************************************************************************
#
#alias EDITOR="nano "
#
##*******************************************************************************************************************************
#
#export RONIN_PATH
#
#source /etc/environment
#
#cat $NOH_ROOT/motd.post
#echo
#
##*******************************************************************************************************************************
#
#if [[ -f $HOME/.shelter/env.sh ]] ; then
#     source $HOME/.shelter/env.sh
#fi
#
#export RONIN_ENV_GEN=$RONIN_BOOT/shell/env.gen
#
#if [[ $"USER" == "root" ]] ; then
#  set >$RONIN_ENV_GEN
#fi

#export PATH="$PATH:$RONIN_ROOT/node_modules/.bin/:$RONIN_SBIN:$RONIN_FILE/scripts:$RONIN_TEMP/scripts"

#for pth in `echo $NLTK_DATA $RONIN_PROG $RONIN_SBIN $RONIN_CODE $RONIN_SITE $RONIN_WORK $RONIN_EXEC/{red,s3,ldf} $RONIN_TEMP/scripts` ; do
#    if [[ ! -d $pth ]] ; then
#        echo mkdir -p $pth
#    fi
#done

