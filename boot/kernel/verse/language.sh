#!/bin/bash

################################################################################

ronin_load_language () {
    lang=$1

    export LANG_NAME=$lang
    export LANG_PATH=$RONIN_CODE/$lang
    export LANG_INCL=$RONIN_INCL/$lang
    export LANG_CORE=$RONIN_CORE/$lang
    export LANG_REPO=https://bitbucket.org/IT-Ronin/$lang.git

    if [[ ! -d $LANG_PATH ]] ; then
        git clone $LANG_REPO $LANG_PATH --recursive
    fi

    for path in $LANG_CORE $LANG_INCL ; do
        if [[ ! -d $path ]] ; then
            mkdir -p $path
        fi
    done

    if [[ -f $LANG_PATH/context.sh ]] ; then
        source $LANG_PATH/context.sh
    fi

    if [[ -f $LANG_PATH/environ.sh ]] ; then
        source $LANG_PATH/environ.sh

        deps=$LANG_DEPEND

        unset LANG_NAME LANG_PATH LANG_INCL LANG_REPO LANG_DEPEND

        if [[ "x"$deps != "x" ]] ; then
            if [[ $(python -c "print '$deps' in '$TARGET_LANG'") == "False" ]] ; then
                ronin_load_language $deps $*
            else
                export TARGET_LANG="$TARGET_LANG $*"
            fi
        else
            export TARGET_LANG="$TARGET_LANG $*"
        fi
    else
        motd_text "(!) Missing specs for language : "$lang
    fi

    unset LANG_NAME LANG_PATH LANG_INCL LANG_REPO LANG_DEPEND
}

################################################################################

ronin_require_language () {
    export RONIN_LANGUAGEs_REQUESTED=("${RONIN_LANGUAGEs[@]}")

    export TARGET_LANG=""

    for lang in ${RONIN_LANGUAGEs[@]} ; do
        ronin_load_language $lang
    done

    export RONIN_LANGUAGEs=( )

    for lang in $TARGET_LANG ; do
        export RONIN_LANGUAGEs=("${RONIN_LANGUAGEs[@]}" $lang)
    done

    for lang in ${RONIN_LANGUAGEs[@]} ; do
        ronin_require_$lang
    done

    export RONIN_LANGUAGEs_SUPPORTED=( )

    for lang in `ls $RONIN_CODE` ; do
        if [[ $(python -c "print '$lang' in '$RONIN_LANGUAGEs'") == "False" ]] ; then
            export RONIN_LANGUAGEs_SUPPORTED=("${RONIN_LANGUAGEs_SUPPORTED[@]}" $lang)
        fi
    done

    unset TARGET_LANG lang
}

#***************************************************************************************

ronin_include_language () {
    motd_text " "
    motd_text "Programming support : "

    for lang in ${RONIN_LANGUAGEs[@]} ; do
        ronin_include_$lang

        #if [[ -d $LANG_PATH/bin ]] ; then
        #    export RONIN_PATH=$LANG_PATH/bin:$RONIN_PATH
        #fi
    done

    source_with manager

    motd_text " "
    motd_text "  => Also supports : "${RONIN_LANGUAGEs_SUPPORTED[@]}
}

