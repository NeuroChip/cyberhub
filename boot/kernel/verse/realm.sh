#!/bin/bash

ronin_build_realm () {
    realm=$1

    for alias in nucleon psyapps bushido enlight unverse dynasty octopus ; do
        if [[ ! -d $RONIN_INCL/python/$alias ]] ; then
            mkdir -p $RONIN_INCL/python/$alias
        fi

        if [[ -f $RONIN_INCL/python/$alias/__init__.py ]] ; then
            rm -f $RONIN_INCL/python/$alias/__init__.py
        fi

        rpath=wrap

        case $alias in
            psyapps)
                rpath=apps
                ;;
            bushido)
                rpath=func/code
                ;;
            enlight)
                rpath=func/flow
                ;;
            unverse)
                rpath=func/bulk
                ;;
            dynasty)
                rpath=func/task
                ;;
            octopus)
                rpath=func/walk
                ;;
        esac

        source=$RONIN_REAL/$realm/$rpath
        target=$RONIN_INCL/python/$alias/$realm

        if [[ -d $RONIN_REAL/$realm ]] ; then
            if [[ -e $target ]] ; then
                rm -f $target
            fi

            if [[ -d $source ]] ; then
                ln -s $source $target
            fi

            if [[ -e $target/$rpath ]] ; then
                rm -f $target/$rpath
            fi

            if [[ -d $target ]] ; then
                if [[ ! -f $target/__init__.py ]] ; then
                    touch $target/__init__.py
                fi

                case $2 in
                    nucleon|psyapps)
                        ;;
                    *)
                        echo "from . import "$realm >>$RONIN_INCL/python/$alias/__init__.py
                        ;;
                esac

                if [[ -f $target/.gitkeep ]] ; then
                    rm -f $target/.gitkeep
                fi
            fi
        fi
    done
}

ronin_install_realm () {
    realm=$1

    for aspect in dataset ontology process viewer ; do
        if [[ ! -d $RONIN_SPEC/$aspect ]] ; then
            mkdir -p $RONIN_SPEC/$aspect
        fi

        for nrw in `ls $RONIN_REAL/$key/spec/$aspect` ; do
            src=$RONIN_REAL/$key/spec/$aspect/$nrw
            dest=$RONIN_SPEC/$aspect/$key.$sub

            if [[ -d $src ]] ; then
                if [[ ! -d $dest ]] ; then
                    ln -s $src $dest
                fi
            fi

            case $aspect in
                dataset)
                    ;;
                ontology)
                    ;;
                process)
                    ;;
                viewer)
                    ;;
            esac
        done
    done
}

################################################################################

ronin_require_realm () {
    if [[ -d $RONIN_ROOT/etc/realms ]] ; then
        rm -fR $RONIN_ROOT/etc/realms/*
    else
        mkdir -p $RONIN_ROOT/etc/realms
    fi

    for key in ${RONIN_REALMs[@]} ; do
        export REALM_NAME=$key
        export REALM_CONF=$RONIN_CONF/realms/$REALM_NAME
        export REALM_PATH=$RONIN_REAL/$REALM_NAME

        if [[ -d $REALM_PATH ]] ; then
            if [[ -f $REALM_PATH/env.sh ]] ; then
                source $REALM_PATH/env.sh
            fi

            if [[ -d $REALM_PATH/mgmt ]] ; then
                export RONIN_PATH=$RONIN_PATH":"$REALM_PATH/mgmt
            fi

            #ronin_build_realm $key

            #ronin_install_realm $key
        fi

        unset REALM_NAME REALM_PATH
    done
}

ronin_include_realm () {
    motd_text " "
    motd_text "Existing realms :"

    for key in ${RONIN_REALMs[@]} ; do
        motd_text "    -> "$key
    done

    #motd_text " "
    #motd_text "Extensions supported :"
    #motd_text "    Systems   : "$RONIN_SYSTEMS
    #motd_text "    Languages : "$RONIN_TONGUES
    #motd_text "    Modules   : "$RONIN_ABILITY
    #motd_text "    Profiles  : "$RONIN_PROFILE
}

