#!/bin/bash

################################################################################

ronin_require_feature () {
    export RONIN_FEATUREs=( )

    for key in `ls $RONIN_ROOT/dev` ; do
        if [[ -e $RONIN_ROOT/dev/$key/detect.sh ]] ; then
            result=$($RONIN_ROOT/dev/$key/detect.sh)
        else
            result="true"
        fi

        if [[ $result == "true" ]] ; then
            export FEATURE_NAME=$key
            export FEATURE_PATH=$RONIN_ROOT/dev/$FEATURE_NAME

            if [[ -f $FEATURE_PATH/environ.sh ]] ; then
                source $FEATURE_PATH/environ.sh
            fi

            if [[ -f $FEATURE_PATH/environ.sh ]] ; then
                source $FEATURE_PATH/environ.sh
            fi

            if [[ -d $FEATURE_PATH/sbin ]] ; then
                RONIN_PATH=$RONIN_PATH":"$FEATURE_PATH/sbin
                RONIN_PATH=$RONIN_PATH":"$FEATURE_PATH/sbin
            fi

            ronin_require_$key

            unset FEATURE_NAME FEATURE_PATH

            export RONIN_FEATUREs=("${RONIN_FEATUREs[@]}" $key)
        fi
    done
}

#***************************************************************************************

ronin_include_feature () {
    motd_text " "
    motd_text "With features :"

    for key in ${RONIN_FEATUREs[@]} ; do
        ronin_include_$key
    done
}

################################################################################################################################

ronin_setup_feature () {
    for key in ${RONIN_FEATUREs[@]} ; do
        ronin_setup_$key
    done
}

