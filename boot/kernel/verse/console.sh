#!/bin/bash

################################################################################

ronin_require_console () {
    export RONIN_SHELL_INVITE=`echo $SHELL | sed -e 's/\/bin\///g'`
    export RONIN_SHELL_FOLDER=$RONIN_BOOT/shell/invite.d/$RONIN_SHELL_INVITE

    if [[ -f $RONIN_SHELL_FOLDER/env.sh ]] ; then
        echo source $RONIN_SHELL_FOLDER/env.sh

        #source $RONIN_SHELL_FOLDER/env.sh
    else
        motd_exit "Unsupported shell interpreter : "$RONIN_SHELL_INVITE
    fi

    #*******************************************************************************
    #*******************************************************************************

    #if [[ ! -d $RONIN_HOME ]] ; then
    #    git clone https://bitbucket.org/IT-Ronin/$PERSONNA.git $RONIN_HOME --recursive
    #fi

    #if [[ ! -d $RONIN_HOME ]] ; then
    #    mkdir -p $RONIN_HOME/.{creds,ssh}
    #fi

    #*******************************************************************************

    #if [[ -f $RONIN_HOME/.profile ]] ; then
    #    source $RONIN_HOME/environ.sh
    #else
    #    #export LANGUAGE=en-US
    #    export APPLETs="connector console devops infosec explor hobbit linguist linked"
    #fi
}

ronin_include_console () {
    motd_text "Ghost detected :"
    motd_text "    Domain : "$RONIN_DOMAIN
    #motd_text "    Narrow : "$RONIN_NARROW

    motd_text '    Organism : '$ORGANISM

    if [[ "x"$IDENTITY != "x"$USER ]] ; then
        motd_text '    Identity : '$IDENTITY
    fi

    if [[ -d $RONIN_HOME ]] ; then
        motd_text '    Person : '$IDENTITY
    fi

    #*******************************************************************************

    #source_deep $RONIN_BOOT/shell/{helper,alias,tools}.d

    source_with aliases

    #*******************************************************************************

    for key in oauth token backend ; do
        target=$RONIN_CRED/$key.sh

        if [[ -f $target ]] ; then
            source $target
        fi
    done
}

