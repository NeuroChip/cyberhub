#!/bin/bash

if [[ "x"$NOH_PROFILE == "x" ]] ; then
    export NOH_PROFILE="gateway"
fi

if [[ "x"$NOH_ASPECTs == "x" ]] ; then
    export NOH_ASPECTs="console system language realm feature"
fi

################################################################################

export RONIN_BOOT=$RONIN_ROOT'/boot'
export RONIN_EXEC=$RONIN_ROOT'/bin'
export RONIN_CONF=$RONIN_ROOT'/etc'
export RONIN_TEMP=$RONIN_ROOT'/tmp'
export RONIN_FILE=$RONIN_ROOT'/usr'
export RONIN_VARI=$RONIN_ROOT'/var'

export RONIN_MONT=$RONIN_ROOT'/mnt'
export RONIN_PROC=$RONIN_ROOT'/proc'
export RONIN_SERV=$RONIN_ROOT'/srv'

export RONIN_DEV_DIR=$RONIN_ROOT'/dev'
export RONIN_LIB_DIR=$RONIN_ROOT'/lib'
export RONIN_SYS_DIR=$RONIN_ROOT'/sys'
export RONIN_ORG_DIR=$RONIN_ROOT'/srv'

#*******************************************************************************

export RONIN_CODE=$RONIN_ROOT'/lib'

################################################################################

if [[ "x"$RONIN_HOME == "x" ]] ; then
    export RONIN_HOME=$RONIN_ROOT/home/$IDENTITY
fi

#*******************************************************************************

if [[ "x"$RONIN_WORK == "x" ]] ; then
    export RONIN_WORK=$RONIN_ROOT/srv/$ORGANISM
fi

#*******************************************************************************

#if [[ "x"$RONIN_SITE == "x" ]] ; then
#    export RONIN_SITE=$RONIN_SERV/domains/$DOMAIN
#fi

################################################################################

export RONIN_CORE=$RONIN_FILE/lib
export RONIN_INCL=$RONIN_FILE/include
export RONIN_BOOK=$RONIN_FILE/catalog

#*******************************************************************************

export RONIN_DIRS=$RONIN_HOME/.local/ronin
export RONIN_CRED=$RONIN_HOME/.creds

#*******************************************************************************

export RONIN_TOOL=$RONIN_EXEC/tools
export RONIN_PROG=$RONIN_EXEC/progs

#*******************************************************************************

export RONIN_REAL=$RONIN_ROOT/realms
export RONIN_SPEC=$RONIN_ROOT/srv/specs

