# Common shell helpers

alias eradicate='killall -s KILL -v'

alias ssh-clear='ssh-keygen -f $HOME/.ssh/known_hosts -R '

alias random='od -vAn -N4 -tu4 < /dev/urandom'
alias pwdgen="< /dev/urandom tr -dc 'A-Za-z0-9\\?!=-_' | head -c13"

# POSIX enhancement

alias reboot="sudo shutdown -r now"
alias :q="exit"

# Shell listing enhancement

alias dir='dir --color=auto'
alias vdir='vdir --color=auto'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias folder="mkdir -p "

alias lh='ll -h'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias lsize='ls -l $1 | wc -l'

#alias ll="ls -lShra "
#alias lt="ls -lthra "

alias dfh="df -h "

diskusage () {
  du -hd1 $* | sort -h
}

# Supervisor's shortcuts

alias superctl='supervisorctl'
alias superstart='supervisorctl start'
alias superrestart='supervisorctl restart'
alias superstop='supervisorctl stop'
alias supertail='supervisorctl tail -f'

# Apache-like shortcuts

alias a2reload='service apache2 reload'

