#!/bin/bash

iface_list () {
    ip address show $1 | grep -e $2 | head -n 1 | sed -e 's/\(.*\)'$2' \(.*\) scope\(.*\)/\2/g'
}

iface_addr () {
    ip address show $1 | grep -e $2 | head -n 1 | sed -e 's/\(.*\)'$2' \(.*\) scope\(.*\)/\2/g'
}

################################################################################

ronin_require_booth () {
    export RONIN_BOOTH_TYPE=shell

    if [[ "x"$DESKTOP_SESSION != "x" ]] ; then
        export RONIN_BOOTH_NAME=$(echo $SHELL | sed -e 's/\/bin\///g')
    else
        export RONIN_BOOTH_NAME=$DESKTOP_SESSION

        case $DESKTOP_SESSION in
            cairo-dock)
                export RONIN_BOOTH_TYPE=gnome
                ;;
        esac
    fi
}

#***************************************************************************************

ronin_include_booth () {
    motd_text "    Booth : "$RONIN_BOOTH_TYPE" ("$RONIN_BOOTH_NAME")"
}

