#!/bin/bash

if [[ x$SPARKLE_TRANSPORT == "x" ]] ; then
    export SPARKLE_TRANSPORT="tor"
fi

case $SHL_OS in
  windows)
    #export CYG_PKGs=$CYG_PKGs",octave,R"
    ;;
#*****************************************************************************************************
  debian|ubuntu)
    export APT_PKGs="$APT_PKGs tor"

    #alias openyourmind='git submodule foreach --recursive git pull -u origin master'
    ;;
#*****************************************************************************************************
  raspbian)
    #export APT_PKGs="$APT_PKGs wvdial hostapd"
    ;;
esac

for transport in $SPARKLE_TRANSPORT ; do
    case $SHL_OS in
      tor)
        export APT_PKGs="$APT_PKGs tor"
        ;;
      ipfs)
        case $SHL_OS in
          windows)
            IPFS_PACKAGE=https://dist.ipfs.io/go-ipfs/v0.4.2/go-ipfs_v0.4.2_linux-386.tar.gz

            if [[ ! -f $CHARM_PATH/sbin/ipfs ]] ; then
                wget -c $IPFS_PACKAGE

                tar zxf go-ipfs_v0.4.2_linux-386.tar.gz

                mv go-ipfs_v0.4.2_linux-386/ipfs $CHARM_PATH/sbin/ipfs

                rm -fR go-ipfs_v0.4.2*
            fi
            ;;
          #*****************************************************************************************************
          debian|ubuntu|raspbian)
            IPFS_PACKAGE=https://dist.ipfs.io/go-ipfs/v0.4.2/go-ipfs_v0.4.2_linux-386.tar.gz

            if [[ $SHL_OS == "raspbian" ]] ; then
                IPFS_PACKAGE=https://dist.ipfs.io/go-ipfs/v0.4.2/go-ipfs_v0.4.2_linux-386.tar.gz
            fi

            if [[ ! -f $CHARM_PATH/sbin/ipfs ]] ; then
                wget -c $IPFS_PACKAGE

                tar zxf go-ipfs_v0.4.2_linux-386.tar.gz

                mv go-ipfs_v0.4.2_linux-386/ipfs $CHARM_PATH/sbin/ipfs

                rm -fR go-ipfs_v0.4.2*
            fi
            ;;
        esac
        ;;
    esac
done

