#!/bin/bash

iface_addr () {
    ip address show $1 2>/dev/null | grep -e $2 | head -n 1 | sed -e 's/\(.*\)'$2' \(.*\) scope\(.*\)/\2/g'
}

################################################################################

ronin_require_transport () {
    export TEREDO_ADDR=$(iface_addr teredo inet6)
}

#***************************************************************************************

ronin_include_transport () {
    export RONIN_TRANSPORTs=( "local" )

    if [[ "x"$TEREDO_ADDR != "x" ]] ; then
        export RONIN_TRANSPORTs=( "$RONIN_TRANSPORTs[@]" "ipv6" )

        motd_text "    Teredo   : "$TEREDO_ADDR
    else
        echo "No IPv6 connection ..."
    fi
}

