#!/bin/bash

export NOH_ARTs=""

list_dir() {
    for key in `ls $1` ; do
        if [[ -d $1/$key ]] ; then
            echo $key
        fi
    done
}

git_addr () {
    if [[ $mode=="ssh" ]] ; then
        echo git@$1:$2.git
    else
        echo https://$1/$2.git
    fi
}

################################################################################

noh_register () {
    local key=$1

    local module=$2
    local target=$3
    local manage=$4
    local prefix=$5

    export_dyn "NOH_ART_${key}_NAME" $module
    export_dyn "NOH_ART_${key}_PATH" $target
    export_dyn "NOH_ART_${key}_TYPE" $manage
    export_dyn "NOH_ART_${key}_ADDR" $prefix
    export_dyn "NOH_ART_${key}_LIST" $(list_dir $target)

    export NOH_ARTs="${NOH_ARTs} ${key}"
}

#*******************************************************************************************************************************

noh_stage () {
    for x in $NOH_ARTs ; do
        local MODULE=$(eval_dyn "NOH_ART_${x}_NAME")
        local TARGET=$(eval_dyn "NOH_ART_${x}_PATH")
        local MANAGE=$(eval_dyn "NOH_ART_${x}_TYPE")
        local PREFIX=$(eval_dyn "NOH_ART_${x}_ADDR")
        local LISTED=$(eval_dyn "NOH_ART_${x}_LIST")
        local ACTIVE=$(eval_dyn "NOH_ART_${x}_CURR")

        case $1 in
            m|mod|module|MODULE|ext|key|name|alias)
                echo $MODULE
                ;;
            p|pth|target|TARGET|dir)
                echo $TARGET
                ;;
            f|mng|manage|MANAGE)
                echo $MANAGE
                ;;
            t|pre|prefix|PREFIX)
                echo $PREFIX
                ;;
            l|lst|listed|LISTED|list)
                echo $PREFIX
                ;;
            a|act|active|ACTIVE|enable|enabled|on)
                echo $PREFIX
                ;;
            *)
                echo $x
                ;;
        esac
    done
}

################################################################################

noh_listing () {
    local MODULE=$(eval_dyn "NOH_ART_${x}_NAME")
    local TARGET=$(eval_dyn "NOH_ART_${x}_PATH")
    local MANAGE=$(eval_dyn "NOH_ART_${x}_TYPE")
    local PREFIX=$(eval_dyn "NOH_ART_${x}_ADDR")
    local LISTED=$(eval_dyn "NOH_ART_${x}_LIST")
    local ACTIVE=$(eval_dyn "NOH_ART_${x}_CURR")

    if [[ -d $TARGET ]] ; then
        echo "NoH listing of '${MODULE}' at : ${TARGET}"

        for x in `ls $TARGET` ; do
            if [[ -d $TARGET/$x ]] ; then
                echo "    -> ${x}"
            fi
        done
    fi
}

#*******************************************************************************************************************************

noh_require () {
    local MODULE=$(eval_dyn "NOH_ART_${x}_NAME")
    local TARGET=$(eval_dyn "NOH_ART_${x}_PATH")
    local MANAGE=$(eval_dyn "NOH_ART_${x}_TYPE")
    local PREFIX=$(eval_dyn "NOH_ART_${x}_ADDR")
    local LISTED=$(eval_dyn "NOH_ART_${x}_LIST")
    local ACTIVE=$(eval_dyn "NOH_ART_${x}_CURR")

    if [[ -d $TARGET ]] ; then
        if [[ -d $TARGET/$2 ]] ; then
            echo "NoH requiring '${2}' of '${MODULE}' at : ${TARGET}/${2}"
        else
            case $MANAGE in
                git)
                    link=$(git_addr bitbucket.org $PREFIX/$2)

                    echo "NoH cloning '${2}' of '${MODULE}' at : ${link}"

                    git clone  $TARGET/$2
                    ;;
                *)
                    #echo "NoH cloning '${2}' of '${MODULE}' at : ${TARGET}/${2}"
                    echo "No method has been defined to manage this art."
                    ;;
            esac
        fi
    fi
}

