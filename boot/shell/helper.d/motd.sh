#!/bin/bash

motd_init () {
    if [[ -f $RONIN_BOOT/cache/motd.exit ]] ; then
        cat $RONIN_BOOT/cache/motd.curr

        motd_exit
    else
        rm -f $RONIN_BOOT/cache/motd.curr

        touch $RONIN_BOOT/cache/motd.curr
    fi
}

motd_exit () {
    if [[ "x"$1 != "x" ]] ; then
        echo $* >>$RONIN_BOOT/cache/motd.exit
    fi

    echo
    echo "Ronin stack exit for the following reason : "$(cat $RONIN_BOOT/cache/motd.exit)
    echo

    read ; exit
}

#*****************************************************************************************************

motd_text () {
    echo $* >>$RONIN_BOOT/cache/motd.curr
    echo $* | $RONIN_ROOT/bin/render motd
}

motd_file () {
    cat $1 >>$RONIN_BOOT/cache/motd.curr
    cat $1
}

#*****************************************************************************************************

motd_begin () {
    motd_init

    motd_file $RONIN_BOOT/kernel/motd.pre
    motd_text " "
}

motd_end () {
    if [[ "x"$1 != "x" ]] ; then
        motd_text " "
        motd_text $*
        motd_text " "
    fi

    motd_file $RONIN_BOOT/kernel/motd.post
    motd_text " "
}

