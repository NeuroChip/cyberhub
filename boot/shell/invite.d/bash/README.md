Bourne Again SHell (BASH) :
===========================

```bash
force_color_prompt=yes

if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi
```
