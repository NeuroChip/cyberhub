#!/bin/bash

export RONIN_CACHE_STATE=y

ronin_cache_init () {
    case $RONIN_CACHE_PROTO in
        file)
            if [[ -f $RONIN_BOOT/cache/environ.diff ]] ; then
                export RONIN_CACHE_READY=y
            else
                export RONIN_CACHE_READY=n
            fi
            ;;
        *)
            export RONIN_CACHE_READY=n
            ;;
    esac
}

ronin_cache_term () {
    case $RONIN_CACHE_PROTO in
        file)
            rm -f $RONIN_BOOT/cache/*.*
            ;;
    esac
}

ronin_cache_load () {
    case $RONIN_CACHE_PROTO in
        file)
            source $RONIN_BOOT/cache/environ.diff

            source $RONIN_BOOT/cache/alias.conf

            cat $RONIN_BOOT/cache/motd.curr
            ;;
    esac
}

ronin_cache_save () {
    case $RONIN_CACHE_PROTO in
        file)
            alias -p >$RONIN_BOOT/cache/alias.conf

            set >$RONIN_BOOT/cache/environ.post

            diff $RONIN_BOOT/cache/environ.{pre,post} | grep -e "^>" | sed -e 's/^> //g' >$RONIN_BOOT/cache/environ.diff
            ;;
    esac
}

