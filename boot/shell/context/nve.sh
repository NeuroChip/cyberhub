APPLETs='connector console'
APP_BOOT=/ron/boot
APP_CODE=/media/tayamino/SYSOP/ROOT/devops/neuro/hub/lib
APP_CRED=/home/tayamino/.creds
APP_DATA=/media/tayamino/SYSOP/ROOT/devops/neuro/hub/srv/tayaa
APP_DIRS=/media/tayamino/SYSOP/ROOT/devops/neuro/hub/storage/tayamino
APP_DISK=/media/tayamino/SYSOP
APP_EXEC=/media/tayamino/SYSOP/ROOT/devops/neuro/hub/program
APP_FILE=/media/tayamino/SYSOP/ROOT/devops/neuro/hub/files
APP_HOME=/media/tayamino/SYSOP/ROOT/devops/neuro/hub/home/tayamino
APP_PROG=/media/tayamino/SYSOP/ROOT/devops/neuro/hub/home/tayamino/progs
APP_ROOT=/ron
APP_SBIN=/media/tayamino/SYSOP/ROOT/devops/neuro/hub/sbin
APP_SITE=/media/tayamino/SYSOP/ROOT/devops/neuro/hub/home/tayamino/sites
APP_TEMP=/media/tayamino/SYSOP/ROOT/devops/neuro/hub/var
APP_VENV=/media/tayamino/SYSOP/ROOT/devops/neuro/hub/var/pyenv/i686
APP_WORK=/media/tayamino/SYSOP/ROOT/devops/neuro/hub/home/tayamino/works
APT_PKGs=' mksh zsh zsh-lovers tree htop fping nmap traceroute'
BREW_PKGs=' mksh'
CATALOGs=/media/tayamino/SYSOP/ROOT/devops/neuro/hub/home/tayamino/usr/catalog
COCKPIT_FUNC_SET='web x11'
CYG_PKGs=,nano,bash,dash,fish,mksh,zsh,tree,htop,nmap
DISKTAPE=SYSOP
DJANGO_SETTINGS_MODULE=reactor.webapp.settings
GDMSESSION=ubuntu
IDENTITY=cyborg
INSTANCE=
JINCHURIKI_PATHs='ipv6 alter vpn'
JINCHURIKI_SHIELD=yes
JINCHURIKI_TAILS=
MINIO_ACCESS_KEY=minio
MINIO_SECRET_KEY=miniostorage
MONGODB_URL=mongodb://localhost:8017/hubot
NOH_ROOT=/ron/usr
NPM_PKGs=' nsh'
ORGANISM=tayaa
PERSONNA=tayamino
RONIN_CHARMS_DIR=/ron/usr/kabuki
RONIN_COOKBOOK=/ron/usr/kyogen
RONIN_HOME_DIR=/home
RONIN_HOME_ROOT=/root
RONIN_LANG_DIR=/ron/lib
RONIN_LANG_SUB=lib
RONIN_OS_DIR=/ron/usr/bunraku
RONIN_PATH=/ron/sbin
RONIN_RECIPE=kyogen
RONIN_SDK_DIR=/ron/usr/sdk
folder=/ron/boot/kernel/wrappers
script=/ron/boot/kernel/wrappers/system.sh
word=ronin_include_recipe
command_not_found_handle () 
{ 
    if [ -x /usr/lib/command-not-found ]; then
        /usr/lib/command-not-found -- "$1";
        return $?;
    else
        if [ -x /usr/share/command-not-found/command-not-found ]; then
            /usr/share/command-not-found/command-not-found -- "$1";
            return $?;
        else
            printf "%s: command not found\n" "$1" 1>&2;
            return 127;
        fi;
    fi
}
commit_all () 
{ 
    deming add --all;
    deming commit -a -m Summarizing...
}
cycle () 
{ 
    git submodule foreach git $*;
    git $*
}
declare_os_specs () 
{ 
    export HELLO=WORLD
}
deming () 
{ 
    for pth in `echo $APP_ROOT{,/{catalog,reactor}} $APP_HOME`;
    do
        cd $pth;
        cycle $*;
    done;
    for key in `echo $APPLETs`;
    do
        cd $APP_ROOT/nucleon/$key;
        cycle $*;
    done;
    cd $APP_ROOT
}
ensure_all () 
{ 
    for key in `echo $APPLETs`;
    do
        ensure_repo 'Application' $key $APP_ROOT/nucleon/$key https://bitbucket.org/NeuroFleet/$key.git;
    done;
    for key in `echo $PROGRAMs`;
    do
        ensure_repo 'Program' $key $APP_ROOT/programs/$key https://bitbucket.org/NeuroFleet/$key.git;
    done
}
ensure_fs_core () 
{ 
    if [[ ! -d $GHOST_SPECS ]]; then
        git clone https://bitbucket.org/$GHOST_NARROW/$GHOST_PERSON.git $GHOST_SPECS --recursive;
    fi
}
ensure_fs_exts () 
{ 
    for ext in `echo $GHOST_REACTOR $GHOST_FRAMES`;
    do
        if [[ ! -d $GHOST_LIB/python/dist/$ext ]]; then
            git clone https://bitbucket.org/NeuroMATE/$ext.git $GHOST_LIB/python/dist/$ext;
        fi;
    done;
    for app in $GHOST_AGENTS;
    do
        if [[ ! -d $GHOST_LIB/python/dist/$GHOST_NARROW/$app ]]; then
            git clone https://bitbucket.org/$GHOST_NARROW/$app.git $GHOST_LIB/python/dist/$GHOST_NARROW/$app;
        fi;
    done
}
ensure_os_specs () 
{ 
    if [[ -f /etc/os-release ]]; then
        source /etc/os-release;
        for KEY in ID ID_LIKE NAME PRETTY_NAME VERSION VERSION_ID HOME_URL SUPPORT_URL BUG_REPORT_URL ANSI_COLOR;
        do
            VALUE=`eval 'echo \$'$KEY`;
            if [[ "$VALUE" != "" ]]; then
                eval 'export GHOST_SYSTEM_'$KEY'="'$VALUE'"';
                eval 'export DISTRIB_'$KEY'="'$VALUE'"';
                unset $KEY;
            fi;
        done;
    fi;
    case $DISTRIB_ID in 
        raspbian)
            export GHOST_SYSTEM="raspbian"
        ;;
        debian | Debian)
            export GHOST_SYSTEM="debian"
        ;;
        ubuntu | Ubuntu)
            export GHOST_SYSTEM="ubuntu"
        ;;
        *)
            case $OSTYPE in 
                cygwin)
                    export GHOST_SYSTEM="windows";
                    export RONIN_HOME_ROOT=$HOME;
                    export RONIN_HOME_DIR=$HOMEDRIVE"\Users"
                ;;
                *)
                    export GHOST_SYSTEM="linux";
                    if [[ -f /usr/bin/yum ]]; then
                        export GHOST_SYSTEM="redhat";
                    fi;
                    if [[ -d /Users ]]; then
                        export GHOST_SYSTEM="macosx";
                        export RONIN_HOME_ROOT=/var/root;
                        export RONIN_HOME_DIR=/Users;
                    fi
                ;;
            esac
        ;;
    esac;
    export GHOST_SYSDIR=$GHOST_FOLDER/boot/os/$GHOST_SYSTEM;
    require_os_specs $GHOST_SYSTEM;
    export OS_NAME=$GHOST_SYSTEM;
    export OS_PATH=$GHOST_SYSDIR;
    for key in ID NAME VERSION ANSI_COLOR;
    do
        if [[ "`eval 'echo \$GHOST_SYSTEM_'$key`" != "" ]]; then
            eval "export OS_$key=\$GHOST_SYSTEM_$key";
        fi;
    done;
    source $GHOST_SYSDIR/env.sh;
    unset OS_NAME OS_PATH;
    for key in ID NAME VERSION ANSI_COLOR;
    do
        if [[ "`eval 'echo \$OS_'$key`" != "" ]]; then
            eval "unset OS_"$key;
        fi;
    done
}
ensure_repo () 
{ 
    if [[ ! -d $3 ]]; then
        git clone $4 $3 --recursive;
    else
        echo $1" '$2' already exists at : "$3;
    fi
}
neurochip_details () 
{ 
    echo;
    echo "Ghost detected :";
    neurochip_infos_ghost;
    echo;
    echo "Using platform :";
    neurochip_infos_platform;
    echo;
    echo "Connected via :";
    neurochip_infos_network;
    echo;
    echo "Extensions supported :";
    echo "    Systems   : "$GHOST_SYSTEMS;
    echo "    Languages : "$GHOST_TONGUES;
    echo "    Modules   : "$GHOST_ABILITY;
    echo "    Profiles  : "$GHOST_PROFILE;
    echo;
    echo "Deployment infos : ";
    neurochip_infos_deploy;
    echo
}
neurochip_infos () 
{ 
    echo;
    echo "Ghost detected :";
    neurochip_infos_ghost;
    echo;
    echo "Using platform :";
    neurochip_infos_platform;
    echo;
    echo "Extensions supported :";
    echo "    Modules   : "$GHOST_ABILITY;
    echo "    Profiles  : "$GHOST_PROFILE;
    echo
}
neurochip_infos_deploy () 
{ 
    echo "    -> Java    : "$CLASSPATH;
    echo "    -> Python  : "$PYTHONPATH;
    echo "    -> Node.js : "$NODE_PATH
}
neurochip_infos_ghost () 
{ 
    echo "    Domain : "$GHOST_DOMAIN;
    echo "    Narrow : "$GHOST_NARROW;
    if [[ "x$GHOST_PERSON" != "x" ]]; then
        echo "    Person : "$GHOST_PERSON;
    fi
}
neurochip_infos_network () 
{ 
    echo "    Teredo   : "$TEREDO_ADDR
}
neurochip_infos_platform () 
{ 
    echo "    Hostname : "`hostname -f`;
    echo "    System   : "$GHOST_SYSTEM;
    echo "    Module   : "$GHOST_MODULE
}
require_os_specs () 
{ 
    export TARGET_KEY=$1;
    export TARGET_DIR=$GHOST_FOLDER/boot/os/$TARGET_KEY;
    if [[ ! -d $TARGET_DIR ]]; then
        git clone https://bitbucket.org/IT-Bushido/$TARGET_KEY.git $TARGET_DIR;
    fi;
    if [[ ! -d $TARGET_DIR ]]; then
        echo "Could'nt find the specs for your Operating System '"$GHOST_SYSTEM"' at : "$GHOST_SYSDIR;
        echo "Sorry, the Shelter can't bootup. Exiting ...";
        exit;
    fi
}
ronin_require_go () 
{ 
    echo hello world > /dev/null
}
ronin_require_java () 
{ 
    echo hello world > /dev/null
}
ronin_require_nodejs () 
{ 
    echo hello world > /dev/null
}
ronin_require_php () 
{ 
    echo hello world > /dev/null
}
ronin_require_python () 
{ 
    for pth in $APP_CODE/python/dist/nucleon $APP_TEMP/pyenv;
    do
        if [[ ! -d $pth ]]; then
            mkdir -p $pth;
        fi;
    done;
    if [[ ! -f $APP_CODE/python/dist/nucleon/__init__.py ]]; then
        touch $APP_CODE/python/dist/nucleon/__init__.py;
    fi;
    for key in reactor;
    do
        pth=$APP_CODE/python/dist/$key;
        if [[ ! -d $pth ]]; then
            git clone https://bitbucket.org/NeuroFleet/$key.git $pth --recursive;
        fi;
    done
}
ronin_require_ruby () 
{ 
    echo hello world > /dev/null
}
ronin_require_shell () 
{ 
    echo hello world > /dev/null
}
runner () 
{ 
    if [[ -f /app/newrelic.ini ]]; then
        newrelic-admin run-program $*;
    else
        exec $*;
    fi
}
symlink () 
{ 
    if [[ ! -d $2 ]]; then
        if [[ ! -f $2 ]]; then
            ln -sf $1 $2;
        fi;
    fi
}
sync_all () 
{ 
    deming pull -u origin master
    deming push -u origin master
}
upgrade_all () 
{ 
    deming pull -u origin master
}

