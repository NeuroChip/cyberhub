CLUTTER_IM_MODULE=xim
DESKTOP_SESSION=ubuntu
GDMSESSION=ubuntu
GDM_LANG=fr_FR
HISTCONTROL=ignoreboth
HOME=/home/tayamino
HOSTNAME=golem
HOSTTYPE=i686
JOB=unity-settings-daemon
LOGNAME=tayamino
NOH_ROOT=/ron/usr
PATH=/home/tayamino/bin:/home/tayamino/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
RONIN_BOOT=/ron/boot
RONIN_CHARMS_DIR=/ron/usr/kabuki
RONIN_COOKBOOK=/ron/usr/kyogen
RONIN_ENV_GEN=/ron/boot/shell/env.gen
RONIN_HOME_DIR=/home
RONIN_HOME_ROOT=/root
RONIN_LANG_DIR=/ron/lib
RONIN_LANG_SUB=lib
RONIN_OS_DIR=/ron/usr/bunraku
RONIN_PATH=/ron/sbin
RONIN_RECIPE=kyogen
RONIN_ROOT=/ron
RONIN_SDK_DIR=/ron/usr/sdk
SESSION=ubuntu
SESSIONTYPE=gnome-session
SHELL=/bin/bash
SSH_AUTH_SOCK=/run/user/1000/keyring/ssh
TERM=xterm-256color
UID=1000
UPSTART_EVENTS='xsession started'
UPSTART_INSTANCE=
UPSTART_JOB=unity7
UPSTART_SESSION=unix:abstract=/com/ubuntu/upstart-session/1000/1303
USER=tayamino
XAUTHORITY=/home/tayamino/.Xauthority
XDG_CONFIG_DIRS=/etc/xdg/xdg-ubuntu:/usr/share/upstart/xdg:/etc/xdg
XDG_CURRENT_DESKTOP=Unity
XDG_DATA_DIRS=/usr/share/ubuntu:/usr/share/gnome:/usr/local/share/:/usr/share/:/var/lib/snapd/desktop
XDG_GREETER_DATA_DIR=/var/lib/lightdm-data/tayamino
XDG_MENU_PREFIX=gnome-
XDG_RUNTIME_DIR=/run/user/1000
XDG_SEAT=seat0
XDG_SEAT_PATH=/org/freedesktop/DisplayManager/Seat0
XDG_SESSION_DESKTOP=ubuntu
XDG_SESSION_ID=c2
XDG_SESSION_PATH=/org/freedesktop/DisplayManager/Session0
XDG_SESSION_TYPE=x11
XDG_VTNR=7
XMODIFIERS=@im=ibus
command_not_found_handle () 
{ 
    if [ -x /usr/lib/command-not-found ]; then
        /usr/lib/command-not-found -- "$1";
        return $?;
    else
        if [ -x /usr/share/command-not-found/command-not-found ]; then
            /usr/share/command-not-found/command-not-found -- "$1";
            return $?;
        else
            printf "%s: command not found\n" "$1" 1>&2;
            return 127;
        fi;
    fi
}

