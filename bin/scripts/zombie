#!/usr/bin/python

from voodoo.helpers import *

#########################################################################################

class ZombieTool(VHostingTool):
    PROG  = 'zombie'
    HELP  = 'Voodoovhost management'

    #************************************************************************************

    def initialize(self):
        self.parser.add_argument('-H', '--vhost', dest='vhost', type=str,
                            default=None, help="vHost to operate on.")

    #************************************************************************************

    def bootstrap(self, *args, **kwargs):
        for nrw in self.cfg:
            vht = Nucleon.get_vhost(self, **nrw)

            vht.persist()

        for pth in ('/ron/usr','/ron'):
            for usr in Nucleon.local.lsdir(pth, 'srv', vHost.SUBDIR_NARROW):
                for key in Nucleon.local.lsdir(pth, 'srv', vHost.SUBDIR_NARROW, usr):
                    if Nucleon.local.exists(pth, 'srv', vHost.SUBDIR_NARROW, usr, '.git'):
                        vht = Nucleon.get_vhost(self, Nucleon.local.join(pth, 'srv', vHost.SUBDIR_NARROW, usr, key), owner=usr, name=key)

                        skip = False

                        for entry in self.cfg:
                            if entry['name']==vht.name and entry['owner']==vht.owner:
                                skip = True

                        if not skip:
                            vht.persist()

                            self.cfg.append(vht)

    #************************************************************************************

    def cleanup(self, *args, **kwargs):
        pass

    #************************************************************************************

    @property
    def context(self):
        return {
            'bpath':   self.bpath,
            'args':    self.args,
            #'runpath': lambda *path: self.rpath('run', *path),
        }

    ###################################################################################

    class Upgrade(vHostDirective):
        HELP = "Initialize a repository with a Demingfile."

        def extend(self):
            self.parser.add_argument('-R', '--recursive', dest='recursive', action='store_true',
                                help="Recursively enhance repositories with Deming.")

        def execute(self, *args, **opts):
            for vht in self.resolve(*keys):
                Nucleon.local.chdir(vht.bpath)

                for key in ['php2use']:
                    if Nucleon.local.exists(vht.bpath, key, '.git'):
                        Nucleon.local.chdir(vht.bpath, key)

                        Nucleon.local.shell('git', 'pull', '-u', 'origin', 'master')

                        Nucleon.local.shell('git', 'submodule', 'update', '--init', '--recursive')

                Nucleon.local.chdir(vht.bpath)

                Nucleon.local.shell('deming', 'ship', '-m', '"Updated the repo through vhostctl from %s ..."' % Nucleon.local.hostname)

            self.do_update(*keys, **opts)

    ###################################################################################

    class Deming(vHostDirective):
        HELP = "Invoke Deming C.I on a vHost query."

        def extend(self):
            self.parser.add_argument('-R', '--recursive', dest='recursive', action='store_true',
                                help="Recursively enhance repositories with Deming.")

        def execute(self, *args, **opts):
            for vht in self.resolve(*keys):
                Nucleon.local.chdir(vht.bpath)

                Nucleon.local.shell('/ron/sbin/deming', action)

    ###################################################################################

    class Fetch(vHostDirective):
        HELP = "Fetch all Git refs in all queryied vHosts."

        def extend(self):
            self.parser.add_argument('-R', '--recursive', dest='recursive', action='store_true',
                                help="Recursively enhance repositories with Deming.")

        def execute(self, *keys, **opts):
            for vht in self.resolve(*keys):
                Nucleon.local.chdir(vht.bpath)

                Nucleon.local.shell('git', 'fetch', '--all')

    ###################################################################################

    class Sync(vHostDirective):
        HELP = "Fetch all Git refs in all queryied vHosts."

        def extend(self):
            self.parser.add_argument('-R', '--recursive', dest='recursive', action='store_true',
                                help="Recursively enhance repositories with Deming.")

        def execute(self, *keys, **opts):
            for vht in self.resolve(*keys):
                Nucleon.local.chdir(vht.bpath)

                Nucleon.local.shell('git', 'pull', 'origin', 'master')

                Nucleon.local.shell('git', 'push', '-u', 'origin', 'master')

            self.do_update(*keys, **opts)

            #self.do_devenv(*keys, **opts)

            #self.do_status(*keys, **opts)

    ###################################################################################

    class Ensure(vHostDirective):
        HELP = "Fetch all Git refs in all queryied vHosts."

        def extend(self):
            self.parser.add_argument('-R', '--recursive', dest='recursive', action='store_true',
                                help="Recursively enhance repositories with Deming.")

        def execute(self, *keys, **opts):
            for vht in self.resolve(*keys):
                if not Nucleon.local.exists(vht.bpath):
                    rmt = vht.repo_links

                    if 'origin' in rmt:
                        del rmt['origin']

                        Nucleon.local.shell('git', 'clone', vht.repo_links['origin'], vht.bpath)

                        if len(rmt):
                            Nucleon.local.chdir(vht.bpath)

                            for name,url in rmt.iteritems():
                                Nucleon.local.shell('git', 'remote', 'add', name, url)

    ###################################################################################

    class Clean(vHostDirective):
        HELP = "Fetch all Git refs in all queryied vHosts."

        def extend(self):
            self.parser.add_argument('-R', '--recursive', dest='recursive', action='store_true',
                                help="Recursively enhance repositories with Deming.")

        def execute(self, *args, **opts):
            for svc in self.daemons:
                svc.clean_config()

    ###################################################################################

    class Devenv(vHostDirective):
        HELP = "Fetch all Git refs in all queryied vHosts."

        def extend(self):
            self.parser.add_argument('-R', '--recursive', dest='recursive', action='store_true',
                                help="Recursively enhance repositories with Deming.")

        def execute(self, *keys, **opts):
            for vht in self.resolve(*keys):
                Nucleon.local.chdir(vht.bpath)

                if vht.repo.flavor:
                    vht.repo.flavor.environment()

    ###################################################################################

    class List(vHostDirective):
        HELP = "Fetch all Git refs in all queryied vHosts."

        def extend(self, *keys):
            self.parser.add_argument('-R', '--recursive', dest='recursive', action='store_true',
                                help="Recursively enhance repositories with Deming.")

        def execute(self, *keys, **opts):
            i = 0

            for vht in self.resolve(*keys):
                if self.args['verbose']:
                    if i!=0:
                        print

                    self.badge_vhost(vht, level=1)

                    i += 1
                else:
                    print ("\t-> %(owner)s/%(name)s" % vht) + ('\t[%s]' % ''.join([
                        iif(cb(vht), key, ' ')
                        for key,tlabel,flabel,cb in self.REPO_FLAGs
                    ]))

    ###################################################################################

    class Status(vHostDirective):
        HELP = "Fetch all Git refs in all queryied vHosts."

        def extend(self):
            self.parser.add_argument('-R', '--recursive', dest='recursive', action='store_true',
                                help="Recursively enhance repositories with Deming.")

        def execute(self, *keys, **opts):
            i = 0

            for vht in self.resolve(*keys):
                if vht.status and vht.repo.dirty:
                    if i!=0:
                        print
                        print "---------------------------------------------------------------------------------------------------------"
                        print

                    Nucleon.local.chdir(vht.bpath)

                    self.badge_vhost(vht, level=6)

                    Nucleon.local.shell('git', 'status')

                    i += 1

            if i==0:
                print "Everything is clean (y)"

    ###################################################################################

    class Dns(vHostDirective):
        HELP = "Fetch all Git refs in all queryied vHosts."

        def extend(self):
            self.parser.add_argument('-R', '--recursive', dest='recursive', action='store_true',
                                help="Recursively enhance repositories with Deming.")

        def execute(self, key, **opts):
            for vht in self.resolve(key):
                for key in diff:
                    if key.startswith('+'):
                        vht.domains.append(key[1:])

                    if key.startswith('-'):
                        if key[1:] in vht.domains:
                            vht.domains -= [key[1:]]

                for fqdn in vht.domains:
                    print "\t-> %s" % fqdn

    ###################################################################################

    class Clone(vHostDirective):
        HELP = "Fetch all Git refs in all queryied vHosts."

        def extend(self):
            self.parser.add_argument('-R', '--recursive', dest='recursive', action='store_true',
                                help="Recursively enhance repositories with Deming.")

        def execute(self, owner, name, **opts):
            pth = Nucleon.local.join('/ron', 'srv', vHost.SUBDIR_NARROW, owner, name)

            if not Nucleon.local.exists(pth):
                Nucleon.local.shell('git', 'clone', 'git@bitbucket.org:%s/vhost-%s.git' % (owner, name), pth)

                vht = Nucleon.get_vhost(self, pth, owner=owner, name=name)

                skip = False

                for entry in self.cfg:
                    if entry['name']==vht.name and entry['owner']==vht.owner:
                        skip = True

                if not skip:
                    vht.persist()

                    self.cfg.append(vht)

    ###################################################################################

    class Enable(vHostDirective):
        HELP = "Fetch all Git refs in all queryied vHosts."

        def extend(self):
            self.parser.add_argument('-R', '--recursive', dest='recursive', action='store_true',
                                help="Recursively enhance repositories with Deming.")

        def execute(self, *keys, **opts):
            for vht in self.resolve(*keys):
                vht.enable()

                print "Enabled vhost '%s' at '%s' succefully." % (vht.narrow, vht.bpath)

    #************************************************************************************

    class Disable(vHostDirective):
        HELP = "Fetch all Git refs in all queryied vHosts."

        def extend(self):
            self.parser.add_argument('-R', '--recursive', dest='recursive', action='store_true',
                                help="Recursively enhance repositories with Deming.")

        def execute(self, *keys, **opts):
            for vht in self.resolve(*keys):
                vht.disable()

                print "Disabled vhost '%s' at '%s' succefully." % (vht.narrow, vht.bpath)

    ###################################################################################

    def do_start(self):
        for svc in self.daemons:
            svc.control('start')

        Nucleon.local.shell('/ron/sbin/homeless', 'telecom')

    #************************************************************************************

    def do_reload(self):
        for svc in self.daemons:
            svc.control('reload')

        Nucleon.local.shell('/ron/sbin/homeless', 'telecom')

    #************************************************************************************

    def do_restart(self):
        for svc in self.daemons:
            svc.control('restart')

        Nucleon.local.shell('/ron/sbin/homeless', 'telecom')

    #************************************************************************************

    def do_stop(self):
        for svc in self.daemons:
            svc.control('stop')

        Nucleon.local.shell('/ron/sbin/homeless', 'telecom')

    #************************************************************************************

    def do_env(self, nrw, target=None, expr=None):
        for vht in self.resolve(nrw):
            for key in vht.env:
                print "%s\t:\t%s" % (key, vht.env[key])

    #************************************************************************************

    def do_run(self, nrw, *command):
        for vht in self.resolve(nrw):
            Nucelon.local.chdir(vht.rpath('.'))

            for key in vht.env:
                os.environ[key] = vht.env[key]

            Nucleon.local.shell(*command)

if __name__=='__main__':
    mgr = ZombieTool()

    mgr(*sys.argv)
