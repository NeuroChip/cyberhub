export LUIGI_HOME=$RONIN_OPT/luigi

export LUIGI_LOGS=$RONIN_VAR/logs/luigi
export LUIGI_TEMP=$RONIN_VAR/run/luigi

export PYTHONPATH=$LUIGI_HOME":"$PYTHONPATH

