#!/bin/bash

symlink $RONIN_ETC/airflow.cfg $AIRFLOW_HOME/airflow.cfg

symlink $RONIN_LIB/python/$RONIN_REACTOR/flow/login $AIRFLOW_HOME/airflow_login

ln -sf $RONIN_LIB/python/$RONIN_REACTOR/flow/dags/*.py $AIRFLOW_HOME/dags/
ln -sf $RONIN_LIB/python/$RONIN_NARROW/*/dags/*.py   $AIRFLOW_HOME/dags/
ln -sf $RONIN_SPECS/airflow/pipelines/*.py      $AIRFLOW_HOME/dags/

ln -sf $RONIN_LIB/python/$RONIN_REACTOR/flow/plugins/*.py $AIRFLOW_HOME/plugins/
ln -sf $RONIN_SPECS/airflow/extensions/*.py        $AIRFLOW_HOME/plugins/

