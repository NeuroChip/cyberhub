#!/bin/bash

export RONIN_TARGET=$AIRFLOW_HOME

if [[ "x"$PORT == "x" ]] ; then export PORT=9400 ; fi

################################################################################

sysV_ctrl () {
    case $1 in
        clean)
            ;;
        build)
            ;;
        release)
            ;;
        ########################################################################
        sync)
            airflow initdb
            ;;
        ########################################################################
        *)
            echo "Not implemented !"
            ;;
    esac
}

