#!/bin/bash

if [[ "x$RONIN_PERSON" == "x" ]] ; then echo "Ghost person not defined. Try : export RONIN_PERSON=proto" ; return ; fi

export RONIN_TARGET=$SOLID_HOME

if [[ "x"$PORT == "x" ]] ; then export PORT=9800 ; fi

################################################################################

export SSL_PREFIX=$RONIN_ETC/ssl/$RONIN_NARROW"-"$RONIN_PERSON

sysV_ctrl () {
    case $1 in
        clean)
            ;;
        build)
            solid init
            ;;
        release)
            ;;
        ########################################################################
        *)
            echo "Not implemented !"
            ;;
    esac
}

