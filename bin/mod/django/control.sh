#!/bin/bash

export GHOST_TARGET=$DJANGO_HOME

if [[ "x"$PORT == "x" ]] ; then export PORT=9200 ; fi

################################################################################

sysV_ctrl () {
    case $1 in
        clean)
            django-admin cleanup
            ;;
        build)
            django-admin setup --build
            ;;
        release)
            django-admin setup --release
            ;;
        ########################################################################
        sync)
            for app in contenttypes sites auth ; do
                django-admin migrate $app --noinput
            done

            django-admin syncdb --noinput

            django-admin createsuperuser --username shivhack --email shivhack@tayaa.me --noinput
            django-admin passwd shivhack --password="Tesla  "
            ;;
        ########################################################################
        shell)
            django-admin shell -i ipython
            ;;
        invite)
            django-admin shell -i bpython
            ;;
        ########################################################################
        *)
            echo "Not implemented !"
            ;;
    esac
}

