export SOLID_HOME=$RONIN_OPT/solid
export SOLID_ROOT=$RONIN_SRV/$RONIN_NARROW/identity

export SOLID_FQDN=idp.$RONIN_DOMAIN
export SOLID_BDIR=$SOLID_ROOT/$SOLID_FQDN

export SSL_VAULT=$RONIN_ETC/ssl/$SOLID_FQDN

################################################################################

if [[ ! -f $SSL_VAULT.key  ]] ; then
    openssl genrsa 2048 > $SSL_VAULT.key
fi

#*******************************************************************************

if [[ ! -f $SSL_VAULT.cert ]] ; then
    openssl req -new -x509 -nodes -sha256 -days 3650 -key $SSL_VAULT.key -subj '/CN=*.vault.'$GHOSTY_DOMAIN > $SSL_VAULT.cert
fi

