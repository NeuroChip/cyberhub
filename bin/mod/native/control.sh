#!/bin/bash

export RONIN_TARGET=$RONIN_BIN/daemons

if [[ "x"$PORT == "x" ]] ; then export PORT=9000 ; fi

################################################################################

sysV_ctrl () {
    case $1 in
        clean)
            echo "Not implemented !"
            ;;
        build)
            echo "Not implemented !"
            ;;
        release)
            echo "Not implemented !"
            ;;
        ########################################################################
        sync)
            echo "Not implemented !"
            ;;
        ########################################################################
        shell)
            bash
            ;;
        invite)
            bpython
            ;;
        ########################################################################
        *)
            echo "Not implemented !"
            ;;
    esac
}
