var dns = require('native-dns');
var server = dns.createServer();

server.on('request', function (request, response) {
  dns.resolve(request.question[0].name, function (err, results) {
    var i;
    if (!err) {
      for (i = 0; i < results.length; i++) {
          response.answer.push(dns.A({
            name: request.question[0].name,
            address: results[i],
            ttl: 600,
          }));
          console.log("proxy : ", request.question[0].name, " with address ", results[i]);
      }
      /*
      response.additional.push(dns.A({
          name: 'hostA.example.org',
          address: '127.0.0.3',
          ttl: 600,
      }));
      //*/
      response.send();
    } else {
      console.log(request,err);
    }
  });
});

server.on('error', function (err, buff, req, res) {
  console.log(err.stack);
});

var port = 53; // parseInt(process.env.PORT || 53);

console.log("Starting the server at port "+port+" :");

server.serve(port);

