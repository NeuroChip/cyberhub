    #!/bin/bash

export HOSTNAME=`hostname`
export PERSONNA=$USER

export TARGET_ARCH=x64
#export TARGET_ARCH=x86

export APP_POOL=native
export APP_ROOT=$PWD

########################################################################################

if [[ ! -f $APP_ROOT/.profile ]] ; then
    echo '#!/bin/bash'                                     >$APP_ROOT/.profile
    echo ''                                               >>$APP_ROOT/.profile
    echo 'export PORT=7000'                               >>$APP_ROOT/.profile
    echo ''                                               >>$APP_ROOT/.profile
    echo 'export REDIS_URL="redis://localhost:6397/0"'    >>$APP_ROOT/.profile
    echo 'export MONGODB_URL="mongodb://localhost/prism"' >>$APP_ROOT/.profile
    echo 'export NEO4J_URL="http://localhost:7474"'       >>$APP_ROOT/.profile
    echo ''                                               >>$APP_ROOT/.profile
    echo 'export APP_LIST="common connector console explor linked hobbit linguist devops infosec"' >>$APP_ROOT/.profile
    echo ''                                               >>$APP_ROOT/.profile
    echo '#export NLTK_CORP=`echo book gutenburg {prop,tree}bank {verb,word}net udhr{,2}`' >>$APP_ROOT/.profile
    echo '##export NLTK_CORP=`echo conll200{0,2,7} city_database dependency_treebank dolch framenet_v1{5,7} genesis knbc names omw pil propbank pros_cons ptb reuters semcor senseval sentence_polarity sentiwordnet state_union shakespeare subjectivity switchboard toolbox words webtext`' >>$APP_ROOT/.profile
    echo '#export NLTK_CORP=`echo book {prop,{,dependency_}tree}bank {name,verb,word}s {verb,{,senti}word}net udhr{,2} conll200{0,2,7} framenet_v1{5,7} city_database dolch genesis gutenburg knbc omw pil pros_cons ptb reuters semcor senseval sentence_polarity state_union shakespeare subjectivity switchboard toolbox webtext`' >>$APP_ROOT/.profile
    echo ''                                               >>$APP_ROOT/.profile
    echo 'export NODE_VERSION="v7.9.0"'                   >>$APP_ROOT/.profile
fi

source $APP_ROOT/.profile

if [[ "x"$PORT == "x" ]] ; then
    export PORT=7000
fi

########################################################################################

export APP_AXIS=$APP_ROOT/realms

export APP_CODE=$APP_ROOT/codes
export APP_TEMP=$APP_ROOT/cache
export APP_PROG=$APP_ROOT/progs
export APP_SERV=$APP_ROOT/serve

export APP_CORP=$APP_TEMP/corpora
export APP_VENV=$APP_TEMP/pyenv
export APP_NODE=$APP_PROG/nodejs

export APP_TEMP_NPM=$APP_TEMP/npm

export PYTHONPATH="$APP_CODE/python"
export NODE_PATH="$APP_CODE/nodejs"

if [[ -d $APP_VENV ]] ; then
    source $APP_VENV/bin/activate
fi

export APP_PATH=$APP_ROOT/script:$APP_ROOT/node_modules/.bin
export APP_PATH=$APP_PATH:$APP_NODE/bin

export PATH=$APP_PATH:$PATH

export NLTK_DATA=$APP_CORP/nltk

########################################################################################

if [[ ! -d $APP_VENV ]] ; then
    virtualenv --distribute $APP_VENV

    $APP_VENV/bin/pip install -r $APP_ROOT/requirements.txt
fi

if [[ -d $APP_VENV ]] ; then
    source $APP_VENV/bin/activate
fi

