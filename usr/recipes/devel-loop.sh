#!/bin/bash

TARGET=~/neuro

BOT_PLUGINs=`echo rediscloud heroku-{redis,postgres} cleardb mlab graphenedb `
BOT_PLUGINs=$BOT_PLUGINs" newrelic logentries"

BOT_DOMAINs="connect"

if [[ ! -d $TARGET ]] ; then
    mkdir -p $TARGET
fi

ensure_bot () {
    pth=$TARGET/$1/$2

    if [[ ! -d $pth ]] ; then
        mkdir -p $pth

        rmdir $pth

        git clone git@bitbucket.org:NeuroMATE/ghost.git $pth --recursive

        git clone git@bitbucket.org:$1/$2.git $pth/home/$2 --recursive

        cd $pth

        heroku apps:create $1"-bot-"$2

        heroku pipelines:add $1

        heroku config:set GHOST_NARROW=$1 GHOST_PERSON=$2 GHOST_DOMAIN=$3

        for plugin in $BOT_PLUGINs ; do
            heroku addons:add $plugin
        done

        for fqdn in $BOT_DOMAINs ; do
            heroku domains:add $fqdn.$3
        done
    fi
}

mainloop () {
    case $1 in
        ensure)
            ensure_bot 'uchikoma' 'conan'   "uchikoma.review"
            ensure_bot 'uchikoma' 'loki'    "uchikoma.loan"
            ensure_bot 'uchikoma' 'max'     "uchikoma.bid"
            ensure_bot 'uchikoma' 'musashi' "uchikoma.webcam"
            ensure_bot 'uchikoma' 'proto'   "uchikoma.faith"

            ensure_bot 'it-samurai' 'alex'    "it-issal.link"
            ensure_bot 'it-samurai' 'betty'   "it-issal.download"
            ensure_bot 'it-samurai' 'jarvis'  "it-issal.review"
            ensure_bot 'it-samurai' 'steve'   "it-issal.top"
            ensure_bot 'it-samurai' 'viki'    "it-issal.pw"
            ;;
        sync)
            ;;
        wipe)
            for name in conan loki max musashi proto ; do
                heroku apps:destroy --app uchikoma-bot-$name --confirm uchikoma-bot-$name

                if [[ -d $TARGET/uchikoma/$name ]] ; then
                    cd $TARGET/uchikoma/$name

                    git remote rm heroku
                fi
            done

            for name in alex betty jarvis steve viki ; do
                heroku apps:destroy --app it-samurai-bot-$name --confirm it-samurai-bot-$name

                if [[ -d $TARGET/uchikoma/$name ]] ; then
                    cd $TARGET/uchikoma/$name

                    git remote rm heroku
                fi
            done
            ;;
    esac
}

mainloop $*

exit

