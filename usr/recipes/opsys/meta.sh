#!/bin/bash

BASE_DIR=$HOME/opsys

TARGET_DIR=$BASE_DIR/srv
VHOST_DIR=$BASE_DIR/vhosts
WARRIOR_DIR=$BASE_DIR/warriors

###################################################################################################

get_contain '/srv/psych'      'bitbucket.org:uchikoma'          'neurotics'      'reflex sirius'
get_contain '/srv/systems'    'bitbucket.org:mozilla-warrior'   'os2use'         'coreos snappy ubuntu zentyal debian centos rhel oracle'
get_contain '/srv/containers' 'bitbucket.org:docker-warrior'    'docker2use'     'phpMyAdmin phpVirtualbox phpRedisAdmin'

###################################################################################################

get_contain '/opt/apache'     'bitbucket.org:apache-warrior'    'apache2use'     'activemq kafka hive hadoop drill phoenix giraph pig spark couchdb cassandra stanbol marmotta'
get_contain '/opt/jboss'      'bitbucket.org:jboss-warrior'     'jboss2use'      'aerogear modeshape datavirt fuse bpm-workbench teiid liferay'
get_contain '/opt/mozilla'    'bitbucket.org:mozilla-warrior'   'mozilla2use'    ''

###################################################################################################

get_contain '/opt/issal/biz'  'bitbucket.org:biz-issal'         'business2use'   'odoo openkm owncloud'
get_contain '/opt/issal/dev'  'bitbucket.org:dev-issal'         'code2use'       'gitlab gerrit jenkins-ci strider-cd sonatype-nexus selenium'
get_contain '/opt/issal/ops'  'bitbucket.org:ops-issal'         'platform2use'   'cacti collectd graphite logstash'
get_contain '/opt/issal/data' 'bitbucket.org:data-issal'        'backends2use'   'rabbitmq zeromq memcached redis mysql postgres oracle-12 mongodb cockroachdb rethinkdb neo4j allegrograph virtuoso rexter-titan elasticsearch voldemort'

#############################################################################

get_meta "marketing" "maher-ops"
get_meta "marketing" "hack2use"
get_meta "marketing" "enochian-designs"
get_meta "marketing" "mokatat-agency"

#****************************************************************************

get_meta "business"  "decotaz"
get_meta "business"  "chez-kim"

#****************************************************************************

get_meta "place"     "cafe-factory"
get_meta "place"     "la-fabrique"
get_meta "place"     "mariuno"
get_meta "place"     "park-adventure"

#****************************************************************************

get_meta "people"    "gali-mahmoud"
get_meta "people"    "laamari-nizar"
get_meta "people"    "lachhab-karim"
get_meta "people"    "tayaa-med-amine"

#****************************************************************************

get_meta "socent"    "inbijas"

#############################################################################

#get_rhc tayaa    antics        '561ecabb2d5271f44900006c'
#get_rhc tayaa    retail        '561ecbf70c1e66f8c20000a6'

#get_rhc touimi   babel         '561ed0f62d527156360001f2'
#get_rhc touimi   tracker       '561ecb8d89f5cf2427000127'
#get_rhc touimi   translate     '561ecb542d52715636000174'

#get_rhc enochian decotaz       '561ecee089f5cfc3e30000bf'
#get_rhc enochian factory       '532c8e8c5973ca67b700016f'
#get_rhc enochian website       '561eced60c1e6639670001b7'

#get_rhc scubadev inbijas       '561ed3830c1e6625e8000001'
#get_rhc scubadev tadart        '561ed39289f5cf4818000007'

#get_rhc takatout mariuno       '561ed1480c1e6667d100028b'
#get_rhc takatout parkadventure '561ed0587628e1667a000129'

#get_rhc eclectic lachhab       '55c19a0a89f5cfc951000121'
#get_rhc eclectic tayamino      '55c1999d89f5cf72ee0000f1'
#get_rhc eclectic laamari       '55c199db7628e183fd000116'

#get_rhc scubaops website       '561f0c447628e1b9d700000c'

