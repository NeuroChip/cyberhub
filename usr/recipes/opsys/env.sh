#!/bin/bash

#############################################################################

OS_LIST="coreos snappy ubuntu zentyal raspbian debian centos rhel oracle osx windows"

STACK_LIST="Ronin Noh Kyogen Qi Shinigami Karate"
LANG_LIST="python php nodejs java arduino dotnet ruby"
CHARM_LIST="deming kungfu hive demantia sparkle teknoman"
APP_LIST="" #"hive demantia sparkle teknoman shinigami"

###################################################################################################

STACK_LIST="Ronin Noh Kyogen Qi Shinigami Karate"
LANG_LIST="python php nodejs java arduino dotnet ruby"
CHARM_LIST="deming kungfu hive demantia sparkle teknoman"
APP_LIST="" #"hive demantia sparkle teknoman shinigami"

###################################################################################################

WARRIOR_DOCKER="neurotics/sirius neurotics/reflex"
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo business2use/{odoo,openkm,owncloud}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo code2use/{gitlab,gerrit,jenkins-ci,strider-cd,sonatype-nexus,selenium}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo platform2use/{cacti,collectd,graphite,logstash}`

WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo backends2use/{{rabbit,zero}mq,memcached,redis,mysql,postgres,oracle-12}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo backends2use/{mongodb,cockroachdb,rethinkdb}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo backends2use/{neo4j,allegrograph,virtuoso,rexter-titan}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo backends2use/{elasticsearch,voldemort}`

WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo jboss2use/{aerogear,modeshape}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo jboss2use/{datavirt,fuse,bpm-workbench}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo jboss2use/{teiid,liferay}`

WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo apache2use/{activemq,kafka}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo apache2use/{hive,hadoop,drill,phoenix,giraph}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo apache2use/{pig,spark}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo apache2use/{couchdb,cassandra}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo apache2use/{stanbol,marmotta}`

# for key in `echo $WARRIOR_DOCKER` ; do docker pull $key ; done

#WARRIOR_DOCKER=$WARRIOR_DOCKER" phpMyAdmin phpVirtualbox phpRedisAdmin"
#WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo google-{appengine,cloud}-sdk`

