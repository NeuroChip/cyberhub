#!/bin/bash

source $HOME/opsys/env.sh

#############################################################################

for stack in $STACK_LIST ; do
    get_repo bitbucket.org:saezuri/$stack.git $TARGET_DIR/stack/$stack
done

for os in $OS_LIST ; do
    get_repo bitbucket.org:saezuri/$os"4noh".git $TARGET_DIR/os/$os
done

#############################################################################

for lang in $LANG_LIST ; do
    get_repo bitbucket.org:saezuri/$lang"2use".git $TARGET_DIR/lib/$lang/dist/$lang"2use"
done

for charm in $CHARM_LIST ; do
    get_repo bitbucket.org:saezuri/$charm.git $TARGET_DIR/charm/$charm

    for lang in $LANG_LIST ; do
        get_repo bitbucket.org:saezuri/$charm"4"$lang.git $TARGET_DIR/lib/$lang/dist/$charm
    done
done

#############################################################################

for app in $APP_LIST ; do
    get_repo bitbucket.org:saezuri/$app"2share".git $TARGET_DIR/apps/$app
done

#############################################################################

TARGET_DIR=$HOME/devel/warriors

for key in $WARRIOR_DOCKER ; do
    get_repo github.com:docker-warrior/$key.git $TARGET_DIR/docker/$key
done

