#!/bin/bash

TARGET=~/devel/neuro

for key in `ls $TARGET/libs` ; do
    cd $TARGET/libs/$key

    git $*
done

for key in `ls $TARGET/apps` ; do
    cd $TARGET/apps/$key

    git $*
done

for key in `ls $TARGET/bots` ; do
    cd $TARGET/bots/$key

    git $*
done

for mod in `ls $TARGET/core` ; do
    cd $TARGET/core/$mod

    git submodule foreach git $*

    git $*
done
