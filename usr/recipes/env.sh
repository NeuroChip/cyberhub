#!/bin/bash

#############################################################################

OS_LIST="coreos snappy ubuntu zentyal raspbian debian centos rhel oracle osx windows"

STACK_LIST="Ronin Noh Kyogen Qi Shinigami Karate"
LANG_LIST="python php nodejs java arduino dotnet ruby"
CHARM_LIST="deming kungfu hive demantia sparkle teknoman"
APP_LIST="" #"hive demantia sparkle teknoman shinigami"

###################################################################################################

STACK_LIST="Ronin Noh Kyogen Qi Shinigami Karate"
LANG_LIST="python php nodejs java arduino dotnet ruby"
CHARM_LIST="deming kungfu hive demantia sparkle teknoman"
APP_LIST="" #"hive demantia sparkle teknoman shinigami"

###################################################################################################

WARRIOR_VAGRANT="scotch/box cloudfoundry/bosh-lite data-science-toolbox/dst roots/bedrock"

WARRIOR_VAGRANT=$WARRIOR_VAGRANT" "`echo ubuntu/{precise,trusty,vivid,wily}{32,64}`
WARRIOR_VAGRANT=$WARRIOR_VAGRANT" "`echo bento/{centos-7.2,fedora-23,debian-8.2,ubuntu-{12,14,15}.04,opensuse-13.2,freebsd-9.3}`

for system in `echo $OS_LIST` ; do
	WARRIOR_VAGRANT="$WARRIOR_VAGRANT os2use/$system"
done

###################################################################################################

WARRIOR_DOCKER="neurotics/sirius neurotics/reflex"

for system in `echo $OS_LIST` ; do
	WARRIOR_DOCKER="$WARRIOR_DOCKER os2use/$system"
done

WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo apache2use/{activemq,kafka}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo apache2use/{hive,hadoop,drill,phoenix,giraph}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo apache2use/{pig,spark}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo apache2use/{couchdb,cassandra}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo apache2use/{stanbol,marmotta}`

WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo jboss2use/{aerogear,modeshape}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo jboss2use/{datavirt,fuse,bpm-workbench}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo jboss2use/{teiid,liferay}`

WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo business2use/{odoo,openkm,owncloud}`

WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo code2use/{gitlab,gerrit,sonatype-nexus}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo code2use/{jenkins-ci,strider-cd}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo code2use/{selenium,}`

WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo backends2use/{{rabbit,zero}mq,memcached,redis,mysql,postgres,oracle-12}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo backends2use/{mongodb,cockroachdb,rethinkdb}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo backends2use/{neo4j,allegrograph,virtuoso,rexter-titan}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo backends2use/{elasticsearch,voldemort}`

WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo daemon2use/{asterisk,collectd,traccar}`

WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo panel2use/{phpMyAdmin,phpVirtualbox,phpRedisAdmin}`
WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo platform2use/{nginx,haproxy,docker-ui,cacti,collectd,graphite,logstash}`

#WARRIOR_DOCKER=$WARRIOR_DOCKER" "`echo google-{appengine,cloud}-sdk`
