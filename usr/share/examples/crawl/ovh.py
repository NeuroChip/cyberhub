# -*- coding: utf-8 -*-

from octopus.shortcuts import *

#****************************************************************************************

class OvhSpider(CrawlSpider):
    name = "ovh"
    allowed_domains = [
        'ovh.com',
        'www.ovh.com',
    ]
    
    start_urls = (
        'http://www.ovh.com/ma/index.xml',
    )
    
    rules = (
        #Rule(LinkExtractor(allow=(r'ma\/domaines\/dot(.*)\.xml', )),                      callback='parse_tld',       follow=True),
        #Rule(LinkExtractor(allow=(r'ma\/hebergement-web\/hebergement-(.*)\.xml', )),      callback='parse_hosting',   follow=True),
        Rule(LinkExtractor(allow=(r'ma\/vps\/(.*)\.xml', )),                              callback='parse_vps'),
        Rule(LinkExtractor(allow=(r'ma\/serveurs_dedies\/', )),                                                       follow=True),
        Rule(LinkExtractor(allow=(r'ma\/serveurs_dedies\/(.*)\/(.*)\.xml', )),            callback='parse_phy'),
    )
    
    def parse_tld(self, response):
        pass
    
    def parse_hosting(self, response):
        pass
    
    def parse_phy(self, response):
        srv = PhysicalServer()
        
        srv['link']        = response.url
        srv['brand']       = 'ovh-dedicated'
        srv['narrow']      = cleanup(response.xpath('//div[contains(@style,"background-color: white;")]/a[1]/p[1]/text()'), sep=None, trim=True, inline=True)
        
        srv['pricing'] = {
            'ontime':  cleanup(response.xpath('//div[@class="full"][3]//div[@class="section2"][2]/div/p/span[1]//text()'), sep='', trim=True, inline=True),
            'monthly': cleanup(response.xpath('//div[@class="full"][3]//div[@class="section2"][2]/div/span[1]//text()'), sep='', trim=True, inline=True),
        }
        
        for key in ('ontime', 'monthly'):
            srv[key] = unicode(srv[key], 'utf-8') + ' Dh'
        
        net = response.xpath('//div[@id="network_content"]//table[@class="full support"][2]')
        
        srv['specs_cpu']   = {
            'raw':       cleanup(response.xpath('//div[@class="full"][3]//table[@class="full noBg"]//tr[1]/td[1]/text()'), sep='', trim=True, inline=True),
        }
        
        srv['specs_ram']   = {
            'raw':       cleanup(response.xpath('//div[@class="full"][3]//table[@class="full noBg"]//tr[2]/td[1]/text()'), sep='', trim=True, inline=True),
        }
        
        srv['specs_hdd']   = {
            'raw':       cleanup(response.xpath('//div[@class="full"][3]//table[@class="full noBg"]//tr[3]/td[1]/text()'), sep='', trim=True, inline=True)
        }
        
        srv['specs_net']   = {
            'bandwidth': {
                'outgowing': cleanup(net.xpath('.//tr[1]/td[1]/text()'), sep='', trim=True, inline=True),
                'burst':     cleanup(net.xpath('.//tr[2]/td[1]/text()'), sep='', trim=True, inline=True),
                'internal':  cleanup(net.xpath('.//tr[3]/td[1]/text()'), sep='', trim=True, inline=True),
                'incoming':  cleanup(net.xpath('.//tr[4]/td[1]/text()'), sep='', trim=True, inline=True),
            },
            'ipv4':      cleanup(response.xpath('//div[@class="full"][3]//table[@class="full noBg"]//tr[4]/td[1]/text()'), sep='', trim=True, inline=True),
            'ipv6':      '/128',
        }
        
        yield srv.cleanup()
    
    def parse_vps(self, response):
        main = response.xpath('//table[contains(@class, "homepage-table")]')
        
        cmpt = main.xpath('.//td[@class="nameOffer"]')
        
        for ind in range(0, len(cmpt)):
            srv = VirtualServer()
            
            srv['link']        = response.url
            srv['brand']       = 'ovh-vps'
            srv['narrow']      = cleanup(main.xpath('.//tr[1]/td[%d]//text()' % (ind+1)), sep=None, trim=True, inline=True)
            
            srv['pricing'] = {
                'ontime':  '0 Dh',
                'monthly': cleanup(main.xpath('.//tr[2]/td[%d]/span[1]//text()' % (ind+1)), sep='', trim=True, inline=True),
            }
            
            srv['pricing']['monthly']    += ' Dh'
            
            srv['specs_cpu']   = {
                'model':     cleanup(main.xpath('.//tr[4]/td/text()'), sep=' ', trim=True, inline=True),
                'raw':       cleanup(main.xpath('.//tr[6]/td[%d]//text()' % (ind+1)), sep=None, trim=True, inline=True),
            }
            
            srv['specs_ram']   = {
                'raw': cleanup(main.xpath('.//tr[7]/td[%d]//text()' % (ind+1)), trim=True, inline=True),
            }
            
            srv['specs_hdd']   = {
                'raw':       cleanup(main.xpath('.//tr[8]/td[%d]//text()' % (ind+1)), trim=True, inline=True)
            }
            
            srv['specs_net']   = {
                'bandwidth': cleanup(main.xpath('.//tr[10]/td/span[1]/text()'), trim=True, inline=True),
                'ipv4':      '/32',
                'ipv6':      '/128',
            }
            
            yield srv.cleanup()

