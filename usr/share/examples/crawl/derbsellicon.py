# -*- coding: utf-8 -*-

from octopus.shortcuts import *

class DerbselliconSpider(CrawlSpider):
    name = "derbsellicon"
    allowed_domains = ["store.derbsellicon.com"]

    start_urls = (
        'http://store.derbsellicon.com/',
    )

    rules = (
        Rule(LinkExtractor(allow=('([\w\d\-\_\/]+)', )),                  callback='parse_category', follow=True),
        Rule(LinkExtractor(allow=('([\w\d\-\_\/]+)\/([\w\d\-\_\/]+)', )), callback='parse_article',  follow=True),
    )

    def parse_category(self, response):
        categ = RetailCategory()

        categ['site']       = self.name
        categ['link']       = response.url
        categ['uid']        = urlparse(response.url).path[1:]

        categ['title']      = cleanup(response.xpath('//div[@id="content"]//h1[1]/text()'), sep=None, trim=True)

        #*****************************************************************************************************************

        categ['products']   = [
            {
                'uid':   cleanup(product.xpath('.//div[@class="cart"]//input/@onclick'), sep=None, trim=True, inline=True).replace("addToCart('",'').replace("');",''),
                'title': cleanup(product.xpath('.//div[@class="name"]/a/text()'), sep=None, trim=True, inline=True),
                'url':   cleanup(product.xpath('.//div[@class="name"]/a/@href'), sep=None, trim=True, inline=True),
                'thumb': cleanup(product.xpath('.//div[@class="image"]//img/@src'), sep=None, trim=True, inline=True),
            }
            for product in response.xpath('//div[@class="product-list"]/div')
        ]

        #*****************************************************************************************************************

        #categ['products']   = response.xpath('').extract()

        yield categ

    def parse_article(self, response):
        entry = RetailArticle()

        entry['link']        = response.url
        entry['site']        = self.name
        entry['uid']         = cleanup(response.xpath('//*[@class="no-display"]/input[@name="product"]/@value'), sep=None, trim=True)

        entry['vendor']      = {}

        entry['title']       = cleanup(response.xpath('//div[@class="description"]/h1/text()'), sep=None, trim=True)
        entry['summary']     = ""
        entry['description'] = cleanup(response.xpath('//*[@class="tab-description"]//*/text()'), sep='\n', trim=True)

        entry['pricing']         = dict(
            per_unit = dict(
                devise = cleanup(response.xpath('//*[@itemprop="currency"]/text()'), sep=None, trim=True),
                value  = cleanup(response.xpath('//*[@itemprop="price"]/text()'), sep=None, trim=True),
            ),
        )

        entry['media'] = {
            'cover':      cleanup(response.xpath('//div[@class="image"]/a[@class="cloud-zoom"]/@href'), sep=None, trim=True),
            'links':      [],
            'thumbnails': [],
        }

        entry['tags'] = [
            {
                'link': a.xpath('./@href').extract(),
                'name': a.xpath('./text()').extract(),
            }
            for x in response.xpath('//div[@class="tags"]/a')
        ]

        yield entry

