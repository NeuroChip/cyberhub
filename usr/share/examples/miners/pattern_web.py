>>> from pattern.web import URL, DOM, plaintext
>>> 
>>> url = URL('http://www.reddit.com/top/')
>>> dom = DOM(url.download(cached=True))
>>> for e in dom('div.entry')[:3]: # Top 3 reddit entries.
>>>     for a in e('a.title')[:1]: # First <a class="title">.
>>>         print repr(plaintext(a.content))
 
u'Invisible Kitty'
u'Naturally, he said yes.'
u"I'd just like to remind everyone that /r/minecraft exists and not everyone wants"
 "to have 10 Minecraft posts a day on their front page."

>>> from pattern.web import Crawler
>>>
>>> class Polly(Crawler): 
>>>     def visit(self, link, source=None):
>>>         print 'visited:', repr(link.url), 'from:', link.referrer
>>>     def fail(self, link):
>>>         print 'failed:', repr(link.url)
>>>
>>> p = Polly(links=['http://www.clips.ua.ac.be/'], delay=3)
>>> while not p.done:
>>>     p.crawl(method=DEPTH, cached=False, throttle=3)
 
visited: u'http://www.clips.ua.ac.be/'
visited: u'http://www.clips.ua.ac.be/#navigation'
visited: u'http://www.clips.ua.ac.be/colloquia'
visited: u'http://www.clips.ua.ac.be/computational-linguistics'
visited: u'http://www.clips.ua.ac.be/contact'

>>> from pattern.web import crawl
>>> 
>>> for link, source in crawl('http://www.clips.ua.ac.be/', delay=3, throttle=3):
>>>     print link
 
Link(url=u'http://www.clips.ua.ac.be/')
Link(url=u'http://www.clips.ua.ac.be/#navigation')
Link(url=u'http://www.clips.ua.ac.be/computational-linguistics') 


