[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

[supervisord]
logfile=%{logdir}/daemon.log
pidfile=%{folder}/daemon.pid
childlogdir=%{logdir}/services

[inet_http_server]
port = 127.0.0.1:%{port}
; username = root
; password = toor

[unix_http_server]
file=%{socket}
chmod=0770
; chown=root:shivhack

[supervisorctl]
serverurl=unix://%{socket}

[include]
files = %{includ}/*.conf

; %{config}/conf.d/*.conf

; %{config}/usr/modules/*/cluster/supervisor.conf

